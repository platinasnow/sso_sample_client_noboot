
var _BLOCKUI_OPTION = { 
	  message: '<img src="/resources/images/dotcomloading.gif" />',
	  overlayCSS: { 
	    backgroundColor: '#fff', 
	    opacity: 0,
	    cursor: 'wait'
	  },
	  css: {
	    top: ($(window).height() - 40) /2 + 'px',
	    left: ($(window).width() - 40) /2 + 'px',
	    width: '40px',
	    border: 'none', 
	    opacity: 0.8,
	    backgroundColor: '#80RRGGBB'
	  }
};