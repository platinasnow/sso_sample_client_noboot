$(function(){
	//gnb
	$(".depth1").find("li").each(function(index, item){
		$(item).mouseenter(function(){
			if($(item).is(".menu6")){
				$(".depth2").addClass("all");
				$(".img_area").hide();
				$(".gnb_depth2").removeClass("on");
				$(".gnb_bg").stop().animate({"height":380}, 500);
				setTimeout(function(){
					$(".depth2 .inner").stop().animate({"height" : 370},300);
				}, 200);
			}else{
				$(".depth2").removeClass("all");
				$(".img_area").show();
				$(".gnb_bg").stop().animate({"height":277}, "linear");
				$(".depth2 .inner").stop().animate({"height" : 240}).find(".gnb_depth2").eq(index).addClass("on").siblings(".gnb_depth2").removeClass("on");
			}
		});
	})
	$("#header").mouseleave(function(){
		$(".depth2").removeClass("all");
		$(".gnb_depth2").removeClass("on");
		$(".depth2 .inner").stop().animate({"height" : 0});
		$(".gnb_bg").stop().animate({"height":0});
	});

	//quick menu
	$("#quick_menu").find("#topBtn").click(function(){
		$("html, body").stop().animate({scrollTop: 0}, 500);
		return false;
	});

	//email selector
	$(".select_email").change(function(){
		var optionSelected = $(".select_email option:selected").val();
		var $emailSelect = $(".email_input"); 
		if(optionSelected == ""){
			$emailSelect.attr("readonly", false).val("");
		}else{
			$emailSelect.attr("readonly", true).val(optionSelected);
		}
	});

	$("#jquery_jplayer_2").jPlayer({
		ready: function () {
			$(this).jPlayer("setMedia", {
				m4v: "/resources/file/brief.mp4",
				poster: "/resources/file/brief.jpg"
			});
		},
		ended: function() { 
			$(this).jPlayer("stop");
		},
		swfPath: "/resources/file/jquery.jplayer.swf",
		supplied: "m4v",
		size: {
			width: "1140",
			height: "560",
			cssClass: ""
		},
		cssSelectorAncestor: "#jp_container_2"
	});
	//List 페이지 검색시 enter로 바로 검색 가능
	$('.search_text').keyup(function(e){
		if(e.keyCode == 13){
			searchList();
		}
	});	
	
	//custom 파일첨부 버튼
	$('body').on('change', '.file_input', function(){
		var text = $(this).val();
		$(this).closest('div').find('.file_text').val(text.substring(text.lastIndexOf('\\')+1, text.length));
	});
	$('.num').keyup(function(){
		var regNumber = /^[0-9]*$/;
		if(!regNumber.test($(this).val())) {
			$(this).val($(this).val().replace(/[^0-9]/g, ""));
			return;
		}
	});
});

//함수계산기
function numberWithCommas(x) {
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

function removeFile($obj, delIdx){
	var delFileTag = '<input type="hidden" name="delIdxs" value="'+delIdx+'"/>';
	$obj.parent().parent().append(delFileTag);
	var obj = $obj;
	//console.log('data-idx' + $obj.parent().attr('data-idx'));
	if($obj.parent().attr('data-idx') == 1){
		$obj.siblings('input').val('');
	}else{
		$obj.parent().remove();	
	}
};

//file type validation
function validationFileTypeSingle(inputFileDoc, typeArray) {
    var $file = $(inputFileDoc);
    var $filePath = $file.val();
    var $fileType = $filePath.substring($filePath.lastIndexOf('.') + 1, $filePath.length);   
    if (typeArray.indexOf($fileType.toLowerCase()) == -1) {
        $file.val('');
        $file.siblings('.bs_file').text('');
        alert('첨부 가능한 파일 형식은 ' + typeArray+' 입니다.');
        return false;
    } else {
        return true;
    }
};
function validationFileType(typeArray) {
    var fileFlag = true;
    $('input[type="file"]').each(function (idx, item) {
        if ($(item).val() != '' && !validationFileTypeSingle($(item), typeArray)) {
            fileFlag = false;
            return false;
        }
    });
    return fileFlag;
};

/* breif */
var sta = true;
var $window = $(window), $htmlBody = $("html, body"),
	$specialReport = $("#specialReport"), $specialInner = $specialReport.find(".special_inner"),
	$specialLi = $specialReport.find(".slide_list").children("li"),
	$specialBox = $specialLi.find(".special_box"), 
	$specialNum = $specialReport.find(".special_num"),
	$specialDim = $(".special_dim"),
	specialCnt = 0, specialNextCnt = 1, specialPrevCnt = $specialLi.length-1,
	specialTime = 800, specialEffect = "easeOutCubic",
	opaTime = 1000, opaEffect = "easeOutCubic";

function specialFirst(){
	$specialLi.eq(0).find(".special_box").animate({"opacity":1},opaTime);
	$specialLi.eq(0).find(".opa1").delay(specialTime).animate({"opacity":1},opaTime,opaEffect).find("img").delay(specialTime+1000).animate({width : 382, opacity:1},opaTime,opaEffect);
	$specialLi.eq(0).find(".opa2").delay(specialTime+1700).animate({"opacity":1},opaTime,opaEffect);
	$specialReport.find(".num").text("01");
}
function opa($obj){
	$obj.find(".special_box").stop().animate({"opacity":1},opaTime);
	$obj.find(".opa1").delay(specialTime).animate({"top":0, "opacity":1},opaTime,opaEffect);
	$obj.find(".opa2").delay(specialTime+600).animate({"top":0, "opacity":1},opaTime,opaEffect);
};
function opaDelay($obj){
	$obj.find(".special_box").delay(specialTime-200).animate({"opacity":1},opaTime);
	$obj.find(".opa1").delay((specialTime*2)-200).animate({"top":0, "opacity":1},opaTime,opaEffect);
	$obj.find(".opa2").delay(((specialTime*2)-200)+600).animate({"top":0, "opacity":1},opaTime,opaEffect);
};

function opaDelaySec2($obj){
	$obj.find(".opa1_1").delay(specialTime).animate({"opacity":1},4000,opaEffect);
};
function opaMany ($obj){
	$obj.find(".special_box").stop().animate({"opacity":1},opaTime);
	$obj.find(".opa1").delay(specialTime).animate({"top":0, "opacity":1},opaTime,opaEffect);
	$obj.find(".opa2").delay(specialTime+600).animate({"top":0, "opacity":1},opaTime,opaEffect);
	$obj.find(".opa3").delay(specialTime+800).animate({"top":0, "opacity":1},opaTime,opaEffect);
	$obj.find(".opa4").delay(specialTime+1000).animate({"top":0, "opacity":1},opaTime,opaEffect);
	$obj.find(".opa5").delay(specialTime+1200).animate({"top":0, "opacity":1},opaTime,opaEffect);
	$obj.find(".opa6").delay(specialTime+1400).animate({"top":0, "opacity":1},opaTime,opaEffect);
	$obj.find(".opa7").delay(specialTime+1600).animate({"top":0, "opacity":1},opaTime,opaEffect);
}

function specialNextMove(){
	sta = false;
	specialPrevCnt = specialCnt;
	$specialLi.eq(specialCnt).stop().animate({"left":"-100%"},specialTime,specialEffect,function(){
		sta = true;
	});
	$specialLi.eq(specialNextCnt).stop().find(".special_box").css({"opacity":0}).end().find(".opa_box").css({"top":"30px" , "opacity":0}).end().find(".opa1_1").css({"opacity":0});
	$specialLi.eq(specialNextCnt).css({"left":"100%"}).stop().animate({"left":0},specialTime,specialEffect);
	if(specialNextCnt == 0){
		opaDelay($specialLi.eq(specialNextCnt));
	}else if(specialNextCnt ==3 || specialNextCnt ==4 || specialNextCnt ==5 || specialNextCnt==9 || specialNextCnt==12 || specialNextCnt==13){
		opaMany($specialLi.eq(specialNextCnt));
	}else{
		opa($specialLi.eq(specialNextCnt));
	};
	if(specialNextCnt ==2){
		opaDelaySec2($specialLi.eq(specialNextCnt));
	}
	specialCnt = specialNextCnt;
	specialNextCnt++;
	if(specialCnt == $specialLi.length-1) specialNextCnt = 0;
	if(specialCnt == $specialLi.length){
		specialCnt = 0;
		specialNextCnt = 1;
	};
	if(specialCnt+1 < 10){
		$specialReport.find(".num").text("0"+(specialCnt+1));
	}else{
		$specialReport.find(".num").text((specialCnt+1));
	}
	$("#specialReport").css("background-image", "url(/resources/images/common/brief_bg_"+ specialCnt +".jpg)");
};
function specialPrevMove(){
	sta = false;
	specialNextCnt = specialCnt;
	$specialLi.eq(specialCnt).stop().animate({"left":"100%"},specialTime,specialEffect,function(){
		sta = true;
	});
	$specialLi.eq(specialPrevCnt).stop().find(".special_box").css({"opacity":0}).end().find(".opa_box").css({"top":"30px" , "opacity":0}).end().find(".opa1_1").css({"opacity":0});
	$specialLi.eq(specialPrevCnt).css({"left":"-100%"}).stop().animate({"left":0},specialTime,specialEffect);

	if(specialPrevCnt == 0){
		opaDelay($specialLi.eq(specialPrevCnt));
	}else if(specialPrevCnt ==3 ||specialPrevCnt ==4 || specialPrevCnt ==5 || specialPrevCnt==9 || specialPrevCnt==12 || specialPrevCnt==13){
		opaMany($specialLi.eq(specialPrevCnt));
	}else{
		opa($specialLi.eq(specialPrevCnt));
	};
	if(specialPrevCnt ==2){
		opaDelaySec2($specialLi.eq(specialPrevCnt));
	};
	specialCnt = specialPrevCnt;
	specialPrevCnt--;
	if(specialCnt == 0){
		specialCnt = 0;
		specialPrevCnt = $specialLi.length-1;
	};
	if(specialCnt+1 < 10){
		$specialReport.find(".num").text("0"+(specialCnt+1));
	}else{
		$specialReport.find(".num").text((specialCnt+1));
	};
	
	$("#specialReport").css("background-image", "url(/resources/images/common/brief_bg_"+ specialCnt +".jpg)")
};
function wScroll(){
	$window.on("mousewheel DOMMouseScroll", function(e){
		if($specialReport.is(".on")){
			var delta = e.originalEvent.wheelDelta;
			if(delta < 0){
				sta && specialNextMove();
			}else{
				sta && specialPrevMove();
			};
			return false;
		};
	});
};
$specialInner.css({"height":$window.height(), "margin-top":-$window.height()/2});
$window.resize(function(){
	//$specialReport.css({"margin-top":-($window.height()/2+3)});
	$specialInner.css({"height":$window.height(), "margin-top":-$window.height()/2});
});
$specialReport.find(".btn_next").click(function(){
	specialNextMove();
});
$specialReport.find(".btn_prev").click(function(){
	specialPrevMove();
});
function briefStart(){
	$htmlBody.addClass("on");
	$specialReport.addClass("on").animate({"width":"100%"}, 700, "easeInOutQuad");
	$specialReport.delay(700).animate({"height":"100%", "margin-top":-($window.height()/2+3)}, 1000, "easeOutQuart");
	$specialDim.addClass("on").animate({"opacity":0.7},700);
	wScroll();
	specialFirst();
	setTimeout(function(){
		$("#jquery_jplayer_2").jPlayer("play");
	}, 1400);
}
function briefReset(){
	specialCnt = 0, specialNextCnt = 1, specialPrevCnt = $specialLi.length-1;
	$window.off("mousewheel DOMMouseScroll");
	$htmlBody.removeClass("on");
	$specialReport.removeClass("on").css({"width":0, "height":0, "margin-top":0});
	$specialDim.removeClass("on").css("opacity",0);
	$specialLi.css("left", "100%");
	$specialLi.eq(0).css("left", 0).find(".special_box").find("h2").find("img").css({width:100, opacity:.5});
	$specialLi.find(".special_box").css({"opacity":0}).find(".opa_box").css({"top":"30px" , "opacity":0});
	$("#specialReport").css("background-image", "url(/resources/images/common/brief_bg_0.jpg)");
}



$(".special_view").click(function(e){
	e.preventDefault();
	briefStart();
});
$(".special_close").click(function(e){
	e.preventDefault();
	briefReset();
});
var $temp = $("#temp");
window.onload = function(){
	for(var i = 0; i < 14; i++){
		var img = new Image();
		img.src = "/resources/images/common/brief_bg_" + i + ".jpg";
		$temp.append(img);
	};
};
var blockUI = function(){
	$("#blockUi").addClass("on");
};
var unblockUI = function () {
    $("#blockUi").removeClass("on");
};
function searchList(){
	$('#page').val(1);
	$('.content form').submit();
};