<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="blockUi">
	<img src="/resources/images/common/loading.gif" alt="">
</div>
	<div id="footer">
		<div class="top">
			<div class="inner">
				<div class="left">
					<ul>
						<li><a href="http://www.daou.co.kr/" target="_blank" title="새창으로열기">회사소개</a></li>
						<li><a href="/sitemap.do">사이트맵</a></li>
						<li><a href="/membership/policy/idc.do">서비스이용약관</a></li>
						<li><a href="/membership/policy/privacy.do">개인정보처리방침</a></li>
						<li><a href="http://www.ftc.go.kr/bizCommPop.do?wrkr_no=&apv_perm_no=2010564001730200114" target="_balnk" title="새창으로열기">사업자 정보확인</a></li>
					</ul>
				</div>
				<div class="right">
					<div class="site_box">
						<a href="javascript:void(0);" class="site_btn">FAMILY SITE</a>
						<div class="inner_pop">
							<ul>
								<li><a href="http://www.daou.com/" target="_blank" title="새창으로열기">다우기술</a></li>
								<li><a href="https://www.halfdomain.co.kr" target="_blank" title="새창으로열기">반값도메인</a></li>
								<li><a href="http://daouoffice.com/" target="_blank" title="새창으로열기">다우오피스</a></li>
								<li><a href="https://www.sabangnet.co.kr/" target="_blank" title="새창으로열기">사방넷</a></li>
								<li><a href="http://www.ppurio.com/mgr/index.qri" target="_blank" title="새창으로열기">뿌리오</a></li>
								<li><a href="https://www.kicassl.com/main/formMain.sg" target="_blank" title="새창으로열기">코모도</a></li>
								<li><a href="https://www.kiwoompay.co.kr/" target="_blank" title="새창으로열기">키움페이</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="bottom">
			<div class="inner">
				<div class="footer_txt">
					<a href="/index.do" class="left"><img src="/resources/images/common/footer_logo.png" alt="DAOU IDC"></a>
					<div class="center">
						<p>(주)다우기술 대표이사 : 김윤덕 | 사업자등록번호:220-81-02810 | 호스팅 서비스 제공자 상호 : (주)다우기술(또는 에스케이브로드밴드㈜)</p>
						<address>주소 : 경기도 용인시 수지구 디지털벨리로 81, 6층(죽전동, 디지털스퀘어) | 전화 : 070-8795-0120 | 통신판매업신고 : 제 2010-용인수지-114호</address>
						<p class="copy">ⓒ 2018 DAOU Tech., INC. All rights reserved.</p>
					</div>
					<div class="right">
						<img src="/resources/images/common/isms_logo.jpg" alt="다우IDC ISMS 인증획득">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="specialReport" class="brief">
		<div class="special_inner">
			<div class="special_slide">
				<ul class="slide_list">
					<li class="list1">
						<div class="special_box">
							<div id="jp_container_2" class="jp-video" style="position:absolute; left:0; top:132px;">
								<div class="jp-type-playlist">
									<div id="jquery_jplayer_2" class="jp-jplayer"></div>
								</div>
							</div>
							<div class="opa_box opa1">
								<h2><img src="/resources/images/common/brief_list1_img_02.png" alt=""></h2>
							</div>
							<div class="opa_box opa2">
								<span class="scroll"><img src="/resources/images/common/brief_list1_img_03.png" alt=""></span>
							</div>
						</div>
					</li>
					<li class="list2">
						<div class="special_box">
							<div class="opa_box opa1">
								<h3>30년 역사의 국내 대표 IT 전문기업<br><strong>다우기술</strong></h3>
								<p>지난 30여 년간 DBMS 대중화와 인터넷 활성화에 앞장선 다우기술.<br>다우기술의 안정된 성장과 다져진 노하우로 시작된 다우IDC는 최고의 기술력으로<br>고객에게 전달할 새로운 가치를 위하여 앞장설 것입니다.</p>
							</div>
							<div class="opa_box opa2">
								<img src="/resources/images/common/brief_list2_img_02.png" alt="">
							</div>
						</div>
					</li>
					<li class="list3">
						<div class="special_box">
							<div class="opa1_1">
								<img src="/resources/images/common/brief_list3_img_01_2.jpg" alt="" />
							</div>
							<div class="opa_box opa1">
								<h3>다우기술이 만든 <br><strong>신뢰의 비즈니스 인프라</strong></h3>
							</div>
							<div class="opa_box opa2">
								<p>다우기술의 30년 기술 노하우가 집약된 다우IDC는<br>고객의 성공적인 비즈니스를 위한 IT기반을 제공합니다.</p>
							</div>
						</div>
					</li>
					<li class="list4">
						<div class="special_box">
							<div class="opa_box opa1">
								<h3>최신 설비 갖춘<br><strong>전문 데이터 센터</strong></h3>
								<p>최첨단 설비와 시스템 도입으로 초고속 네트워크,<br>최적의 서버 환경을 유지하고 있으며<br>최신식 UPS 및 자가발전설비로 안정적이고<br>효율적인 서비스를 제공해 드립니다.</p>
							</div>
							<div class="opa_box opa2">
								<ul>
									<li class="list1 opa_box opa3">
										<span>가용성 보장</span>Tier 3 수준 구축으로<br>신뢰성을 확보하고<br>있습니다. (일부 Tier 2)
									</li>
									<li class="list2 opa_box opa4">
										<span>전력 설비</span>수전 이중화와 무정전<br>설비로 안정적인 환경을<br>구축하고 있습니다.
									</li>
									<li class="list3 opa_box opa5">
										<span>공조 설비</span>설비의 N+1 구성으로<br>안정성을 확보하고<br>있습니다.
									</li>
								</ul>
							</div>
						</div>
					</li>
					<li class="list5">
						<div class="special_box">
							<div class="opa_box opa1">
								<h3>서울 중심에 위치한<br><strong>유일한 IDC</strong></h3>
								<p>다우IDC는 마포구에 1센터, 서초구 SKB IDC에 2센터를 운영하고 있습니다.<br>마포 센터는 서울 중심부인 마포에 위치하여,<br>여의도 및 종로와의 접근성이 매우 뛰어납니다.</p>
							</div>
							<div class="opa_box opa2">
								<ul>
									<li>
										<span class="list_tit" class="opa_box opa3">마포구 <span>1센터</span></span>
										<ul class="opa_box opa4">
											<li><span class="left_cont">서울시청</span><span class="right_cont">3.5 <small>km</small></span></li>
											<li><span class="left_cont">신용산역</span><span class="right_cont">2.2 <small>km</small></span></li>
											<li><span class="left_cont">여의도역</span><span class="right_cont">3.4 <small>km</small></span></li>
										</ul>
									</li>
									<li>
										<span class="list_tit" class="opa_box opa4">서초구 <span>2센터</span></span>
										<ul class="opa_box opa4">
											<li><span class="left_cont">교대역</span><span class="right_cont">267 <small>m</small></span></li>
											<li><span class="left_cont">서초역</span><span class="right_cont">219 <small>m</small></span></li>
										</ul>
									</li>
								</ul>
							</div>
						</div>
					</li>
					<li class="list6">
						<div class="special_box">
							<div class="opa_box opa1">
								<h3>DDos 공격 대응<br><strong>지속적 방어 시스템 구축</strong></h3>
							</div>
							<div class="opa_box opa2">
								<p>엄격한 보안 기술력으로 안전한 인터넷 비즈니스를 약속합니다.</p>
							</div>
							<div class="opa_box opa3">
								<img src="/resources/images/common/brief_list6_img_02.png" alt="타 고객사의 공격 및 고객사를 대상으로 한 공격에도 서비스에 영향이 없습니다." />
							</div>
							<div class="opa_box opa4">
								<img src="/resources/images/common/brief_list6_img_03.png" alt="TCP DDos 공격의 경우 자체 DDos 방어 시스템을 통해 방어합니다." />
							</div>
							<div class="opa_box opa5">
								<img src="/resources/images/common/brief_list6_img_04.png" alt="대용량 UDP DDos 공격의 경우 기간사의 클린존 서비스를 통해 최대 80G 까지 방어합니다." />
							</div>
						</div>
					</li>
					<li class="list7">
						<div class="special_box">
							<div class="opa_box opa1">
								<h3>24시간 365일<br><strong>완벽한 기술지원 시스템</strong></h3>
							</div>
							<div class="opa_box opa2">
								<p>서버의 트래픽 및 시스템 모니터링 뿐만 아니라 웹 및 스토리지의<br>인터넷 회선 분리를 통해 안정적인 서비스를 제공하며,<br>케이블링, OS인스톨, 시스템 장애처리 등도 무상 서비스로 제공합니다.</p>
							</div>
						</div>
					</li>
					<li class="list8">
						<div class="special_box">
							<div class="opa_box opa1">
								<h3>고객의 성공을 위한<br><strong>맞춤 솔루션 제공</strong></h3>
								<p>많은 레퍼런스 업체에 대한 운영 경험을 토대로 고객에게 최적의 기술지원 및 최고의 서비스를 제공합니다.</p>
							</div>
							<div class="opa_box opa2">
								<img src="/resources/images/common/brief_list8_img_02.png" alt="">
							</div>
						</div>
					</li>
					<li class="list9">
						<div class="special_box">
							<div class="opa1_wrap">
								<div class="opa_box opa1">
									<img src="/resources/images/common/brief_list9_img_02.png" alt="코로케이션 서비스">
								</div>
							</div>
							<div class="opa_box opa2">
								<h3>합리적 비용으로 누리는<br><strong>최적의 서비스</strong></h3>
								<p>다우IDC 데이터 센터에서 제공되는<br>초고속 네트워크, 전용 공간, 기술 지원 서비스를 통해<br>합리적인 비용으로 서비스를 운영하실 수 있습니다.</p>
							</div>
						</div>
					</li>
					<li class="list10">
						<div class="special_box">
							<div class="opa_wrap">
								<div class="opa_box opa1">
									<h3>다우IDC<br><strong>주요 서비스</strong></h3>
									<p>다우IDC는 최신 인프라 설비를<br>바탕으로 지속 가능한 기업으로써<br>업계 최고의 서비스를 제공하고<br>있습니다.</p>
								</div>
							</div>
							<ul>
								<li class="opa_box opa2"><a href="/idc-service/colocation.do" target="_blank" title="새창으로열기"><img src="/resources/images/common/brief_list10_img_02.png" alt="코로케이션 바로가기" /></a></li>
								<li class="opa_box opa3"><a href="/idc-service/hosting.do" target="_blank" title="새창으로열기"><img src="/resources/images/common/brief_list10_img_03.png" alt="호스팅 바로가기" /></a></li>
								<li class="opa_box opa4"><a href="/cloud-service/server.do" target="_blank" title="새창으로열기"><img src="/resources/images/common/brief_list10_img_04.png" alt="클라우드 바로가기" /></a></li>
								<li class="opa_box opa5"><a href="/additional-service/cdn.do" target="_blank" title="새창으로열기"><img src="/resources/images/common/brief_list10_img_05.png" alt="CDN 바로가기" /></a></li>
								<li class="opa_box opa6"><a href="/additional-service/mobile.do" target="_blank" title="새창으로열기"><img src="/resources/images/common/brief_list10_img_06.png" alt="모바일 바로가기" /></a></li>
								<li class="opa_box opa7"><a href="/additional-service/security.do" target="_blank" title="새창으로열기"><img src="/resources/images/common/brief_list10_img_07.png" alt="부가서비스 바로가기" /></a></li>
							</ul>
						</div>
					</li>
					<li class="list11">
						<div class="special_box">
							<div class="opa_box opa1">
								<h3>다우기술이 자체 개발한<br><strong>클라우드 서비스</strong></h3>
								<p>다우클라우드는 최신 다우IDC에 안정성을 강화한 플랫폼으로<br>개발되어 언제 어디서나 곧바로 IT자원을<br>필요한 만큼 사용할 수 있는 서비스입니다.</p>
							</div>
							<div class="opa_box opa2">
								<ul>
									<li><img src="/resources/images/common/brief_list11_img_02.png" alt="" /></li>
									<li><img src="/resources/images/common/brief_list11_img_03.png" alt="" /></li>
									<li><img src="/resources/images/common/brief_list11_img_04.png" alt="" /></li>
								</ul>
							</div>
						</div>
					</li>
					<li class="list12">
						<div class="special_box">
							<div class="opa_box opa1">
								<h3>품질 및 성능 검증을 통해<br><strong>인정받은 클라우드</strong></h3>
								<p>하드웨어, 네트워크, 스토리지부터 모니터링까지 고객에게<br>맞춘 최적의 인프라서비스(laas)를 제공하고 있습니다.</p>
							</div>
							<div class="opa_box opa2">
								<img src="/resources/images/common/brief_list12_img_02.png" alt="Data Backup - 24Hours Monitoring - Safety" />
								<img src="/resources/images/common/brief_list12_img_03.png" alt="Safety" />
							</div>
						</div>
					</li>
					<li class="list13">
						<div class="special_box">
							<div class="opa_box opa1">
								<h3>쉽고, 빠르고,<br><strong>경제적인 클라우드</strong></h3>
								<p>고객이 원하는 만큼의 자원을 효율적으로 임차하여<br>사용하는 편리한 서비스를 제공하고 있습니다. </p>
							</div>
							<div class="opa_box opa2">
								<a href="/cloud-service/calculator.do" target="_blank" title="새창으로열기"><img src="/resources/images/common/brief_list13_img_02.jpg" alt="" /></a>
							</div>
							<ul>
								<li>
									<span class="opa_box opa3 opa_left" ><img src="/resources/images/common/brief_list13_img_03.png" alt="효율성 - 3중화 관리 스토리지, 안정화 기간 제공" /></span>
									<span class="opa_box opa3 opa_right" ><img src="/resources/images/common/brief_list13_img_04.png" alt="비용절감 - 사용한 만큼한 정산되는 종량제 시스템" /></span>
								</li>
								<li>
									<span class="opa_box opa4 opa_left" ><img src="/resources/images/common/brief_list13_img_05.png" alt="편의성 - 사용자 콘솔에서 자원 " /></span>
									<span class="opa_box opa4 opa_right" ><img src="/resources/images/common/brief_list13_img_06.png" alt="시간절감 - 5분 이내 자원 할당 가능" /></span>
								</li>
								<li>
									<span class="opa_box opa5 opa_left" ><img src="/resources/images/common/brief_list13_img_07.png" alt="가용자 - 99%의 SAL 보장" /></span>
									<span class="opa_box opa5 opa_right" ><img src="/resources/images/common/brief_list13_img_08.png" alt="기술지원 - 1:1 전담 엔지니어 관리 시스템" /></span>
								</li>
							</ul>
						</div>
					</li>
					<li class="list14">
						<div class="special_box">
							<div class="opa_box opa1">
								<h3>고객 환경에 맞추어<br><strong>다양하게 활용 가능한 클라우드</strong></h3>
								<p>스트리밍 서비스부터 게임플랫폼, 스타트업 등 활용된<br>다양한 방안을 제공하고 있습니다.</p>
							</div>
							<div class="opa_wrap">
								<ul>
									<li class="opa_box opa2"><img src="/resources/images/common/brief_list14_img_02.png" alt="스트리밍 서비스 - 트랙픽 급증 시 빠른 서버 증설" /></li>
									<li class="opa_box opa3"><img src="/resources/images/common/brief_list14_img_03.png" alt="기업 인프라 통합 - 인프라 통합을 위한 유연한 사설 네트워크" /></li>
									<li class="opa_box opa4"><img src="/resources/images/common/brief_list14_img_04.png" alt="게임 플랫폼 - 폭발적으로 증가한 사용자로 인한 트래픽에 효과적" /></li>
									<li class="opa_box opa5"><img src="/resources/images/common/brief_list14_img_05.png" alt="대용량 콘텐츠 - 물리적인 제한 없이 원하는 수준의 스트로지 용량 확보" /></li>
									<li class="opa_box opa6"><img src="/resources/images/common/brief_list14_img_06.png" alt="스타트업/R&D 초기 구축비용 절감" /></li>
								</ul>
							</div>
						</div>
					</li>
					<li class="list15">
						<div class="special_box">
							<div class="opa_box opa1">
								<h3>다우기술의 30년 노하우로<br>최신 비즈니스 인프라 서비스를 제공하는<br>다우IDC를 경험하세요.</h3>
							</div>
							<div class="opa_box opa2">
								<img src="/resources/images/common/brief_list15_img_02.png" alt="DAOU IDC logo" />
								<a href="/support/contact.do" target="_blank" title="새창으로열기"><img src="/resources/images/common/brief_list15_img_03.png" alt="IDC/클라우드 문의하기" /></a>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<div class="special_num">
				<a href="javascript:void(0);" class="btn_prev"><img src="/resources/images/index/btn_arrow_l.png" alt="왼쪽 버튼"></a><span class="num">01</span><span class="total">15</span><a href="javascript:void(0);" class="btn_next"><img src="/resources/images/index/btn_arrow_r.png" alt="오른쪽 버튼"></a>
			</div>
			<a href="javascript:void(0);" class="special_close"><img src="/resources/images/index/btn_close.png" alt="닫기"></a>
		</div>
	</div>
	<div class="special_dim"></div>
	<div id="temp" style="display:none;"></div>
	<!-- ga -->
	<!-- //ga -->
<script>
if('${msg}' != ''){
	alert('${msg}');
}
</script>