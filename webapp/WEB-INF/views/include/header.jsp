<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<div id="header">
	<div class="header_wrap">
		<div class="header_inner">
			<h1 class="logo"><a href="/index.do">DAOU IDC</a></h1>
			<div id="gnb">
				<div class="depth1">
					<ul>
						<li class="menu1"><a href="#">소개</a></li>
						<li class="menu2"><a href="#">IDC</a></li>
						<li class="menu3"><a href="#">클라우드</a></li>
						<li class="menu4"><a href="#">부가서비스</a></li>
						<li class="menu5"><a href="#">고객센터</a></li>
						<li class="menu6"><a href="#">전체메뉴</a></li>
					</ul>
				</div>
				<div class="depth2">
					<div class="inner">
						<div class="gnb_depth2 menu1">
							<div class="gnb_txt">
								<h2>소개</h2>
								<p>비즈니스를 위한 IT기반,<br> 다우IDC를 소개합니다.</p>
							</div>
							<ul>
								<li><a href="/about/intro.do">다우IDC</a></li>
								<li><a href="/about/service-finder.do">맞춤 서비스 찾기</a></li>
							</ul>
							<ul>
								<li><a href="/about/usercase.do">고객 현황 및 사례</a></li>
								<li><a href="/about/brief.do" class="special_view">다우IDC Brief</a></li>
							</ul>
							<div class="img_area">
								<img src="/resources/images/common/gnb_img1.jpg" alt="고객님께 필요한 서비스를 찾아드립니다!">
								<a href="/about/service-finder.do" class="commonBtn1 type1">맞춤 서비스 찾기</a>
							</div>
						</div>
						<div class="gnb_depth2 menu2">
							<div class="gnb_txt">
								<h2>IDC</h2>
								<p>안정적이고 전문적인 서버관리,<br> 다우IDC를 경험하세요.</p>
							</div>
							<ul>
								<li><a href="/idc-service/colocation.do">코로케이션</a></li>
								<li><a href="/idc-service/hosting.do">서버 호스팅</a></li>
								<li><a href="/idc-service/system-magnagement.do">시스템 관리 서비스</a></li>
							</ul>
							<div class="img_area">
								<img src="/resources/images/common/gnb_img2.jpg" alt="서버 호스팅 서비스를 신청해 보세요!">
								<a href="/idc-service/hosting.do" class="commonBtn1 type1">서버 호스팅 신청</a>
							</div>
						</div>
						<div class="gnb_depth2 menu3">
							<div class="gnb_txt">
								<h2>클라우드</h2>
								<p>합리적인 가격으로<br>최고의 클라우드 서비스를 경험하세요.</p>
							</div>
							<ul>
								<li><a href="/cloud-service/server.do">클라우드 서버</a></li>
								<li><a href="/cloud-service/storage.do">클라우드 스토리지</a></li>
								<li><a href="/cloud-service/area.do">활용방안</a></li>
							</ul>
							<ul>
								<li><a href="/cloud-service/pricing.do">서비스 요금</a></li>
								<li><a href="/cloud-service/calculator.do">요금 계산기</a></li>
							</ul>
							<div class="img_area">
								<img src="/resources/images/common/gnb_img3.jpg" alt="합리적인 가격의 서비스 요금을 확인해 보세요!">
								<a href="/cloud-service/calculator.do" class="commonBtn1 type1">요금 계산기</a>
							</div>
						</div>
						<div class="gnb_depth2 menu4">
							<div class="gnb_txt">
								<h2>부가서비스</h2>
								<p>IT 인프라부터 협업 솔루션까지<br>다양한 서비스를 제공합니다.</p>
							</div>
							<ul class="about">
								<li><a href="/additional-service/cdn.do">CDN</a></li>
								<li><a href="/additional-service/mobile.do">모바일</a></li>
								<li><a href="/additional-service/security.do">보안 서비스</a></li>
							</ul>
							<div class="out_link">
								<ul>
									<li><a href="https://www.halfdomain.co.kr/index" target="_blank">도메인</a></li>
									<li><a href="http://daouoffice.com/" target="_blank">그룹웨어</a></li>
									<li><a href="https://www.sabangnet.co.kr/" target="_blank">쇼핑몰관리솔루션</a></li>
								</ul>
								<ul>
									<li><a href="http://www.ppurio.com/mgr/index.qri" target="_blank">문자발송 서비스</a></li>
									<li><a href="https://www.kicassl.com/main/formMain.sg" target="_blank">SSL인증서</a></li>
									<li><a href="https://www.kiwoompay.co.kr/" target="_blank">PG서비스</a></li>
								</ul>
							</div>
						</div>
						<div class="gnb_depth2 menu5">
							<div class="gnb_txt">
								<h2>고객센터</h2>
								<p>365일 24시간, 분야별 전문가가<br> 빠르고 친절하게 답변해 드립니다.</p>
							</div>
							<ul>
								<li><a href="/support/news-list.do">뉴스&이벤트</a></li>
								<li><a href="/support/contact.do">상담신청</a></li>
								<li><a href="/support/faq.do">자주 묻는 질문</a></li>
							</ul>
							<ul>
								<li><a href="/support/service-guide-list.do">서비스 이용 가이드</a></li>
								<li><a href="/support/brochure-download.do">소개자료 다운로드</a></li>
							</ul>
							<div class="img_area">
								<img src="/resources/images/common/gnb_img5.jpg" alt="편리하고 빠르게 상담을 신청해 보세요!">
								<a href="/support/contact.do" class="commonBtn1 type1">상담신청</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<ul class="member">
			<sec:authorize access="isAnonymous()">
				<li><a href="/sso">로그인</a></li>
				<li class="line"><a href="/membership/join/type-selection.do">회원가입</a></li>
				<li><a href="/membership/login.do" class="commonBtn1 type2 etc">Console</a></li>
			</sec:authorize>
			<sec:authorize access="isAuthenticated()">
				<li><sec:authentication property="principal.id"/>님</li>
				<li class="line"><a href="javascript:void(0);" onclick="logout();">로그아웃</a></li>
				<li class="line"><a href="/mypage/technical-support/list.do">마이페이지</a></li>
				<li><a href="javascript:void(0);" id="console" class="commonBtn1 type2 etc" onclick="consoleApi();">Console</a></li>
			</sec:authorize>
				
			</ul>
			<form id="logout-form" method="post" action="/doLogout">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
			</form>
		</div>
	</div>
	
	<div class="gnb_bg"></div>
</div>
<!-- 콘솔팝업 -->
<div class="console_pop ${isConsole }">
	<div class="cs_inner">
		<p>콘솔 접근이 불가능한 계정입니다.<br>클라우드 사용을 원하시면 문의해 주세요.</p>
		<div class="cont">
			<span>상담 문의</span>	
			<span>070.8796.2529</span>	
		</div>
		<a href="/support/contact.do" class="commonBtn3 type2">상담 신청</a>
	</div>
	<a href="javascript:void(0);" class="cs_close" onclick="onClosePopup();"><img src="/resources/images/etc/ico_close.gif" alt="팝업창 닫기"></a>
</div>
<!-- //콘솔팝업 -->
<script>
function logout(){
	$('#logout-form').submit();
};
function onClosePopup(){
	$(".console_pop").removeClass("on");
};
function consoleApi(){
	$.ajax({
		type : 'POST',
		url : '/cloud/cloud-console.do',
		data : {'${_csrf.parameterName}' : '${_csrf.token}' },
		dataType : 'json',
	    timeout : 10000,
	    cache : false,
	    beforeSend : function(){
	    	blockUI();
	    },
		success : function(data){
			if(data.cloudYn == 'Y' && data.cloudUrl != ''){
				window.open(data.cloudUrl, 'console');
			}else{
				$(".console_pop").addClass("on");
			}
		},
		error : function(error){
		},
		complete : function(){
			unblockUI();
		}
	});
}
</script>