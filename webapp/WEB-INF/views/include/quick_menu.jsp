<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="quick_menu">
	<ul>
		<li class="quick_list1"><a href="/support/contact.do">상담<br>신청</a></li>
		<li class="quick_list2"><a href="/mypage/technical-support/list.do">영업<br>기술<br>지원</a></li>
		<li class="quick_list3"><a href="/mypage/visiting-request/list.do">방문<br>요청</a></li>
		<li class="quick_list4"><a href="/about/service-finder.do">맞춤<br>서비스</a></li>
	</ul>
	<a href="#" id="topBtn" class="btn_top">top</a>
</div>