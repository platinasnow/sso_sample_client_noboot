<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
		<div class="lnb_wrap">
			<h3>마이페이지</h3>
			<ul id="lnb" class="lnb">
				<li><a href="/mypage/technical-support/list.do">영업/기술 지원</a></li>
				<li><a href="/mypage/visiting-request/list.do">방문요청</a></li>
				<li><a href="/mypage/carrying-equipment/list.do">장비 반입/반출</a></li>
				<li><a href="/mypage/traffic/traffic-view.do">트래픽 현황</a></li>
				<li><a href="/mypage/billing/list.do">청구서 관리</a></li>
				<li><a href="/mypage/authorization-info.do">회원정보 변경</a></li>
				<li><a href="/mypage/change-password.do">비밀번호 변경</a></li>
				<li><a href="/mypage/leave-authorization.do">회원탈퇴</a></li>
			</ul>
		</div>