<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="include/top.jsp" %>
<link rel="stylesheet" href="/resources/css/plugin/swiper.css">
<link rel="stylesheet" href="/resources/css/index.css">
<title>DAOU IDC</title>
</head>

<body>
<!-- wrap -->
<div id="wrap" class="index">
	<!-- gnb -->
	<%@ include file="include/header.jsp" %>
	<!-- //gnb -->
	<div id="container">
		<%@ include file="include/quick_menu.jsp" %>
		<div class="sec1">
			<div class="inner">
				<p class="sec1_top_txt">다우기술의 30년 노하우로<br><span>최신 인프라 서비스를 제공하는<br>다우IDC를 경험하세요.</span></p>
				<div class="sec1_banner">
					<div class="swiper-container swiper-container-1">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
								<div class="txt_box">
									<h3>다우IDC Brief</h3>
									<p>다우기술이 만든 신뢰의 비즈니스</p>
									<a href="#" class="special_view">바로가기</a>
								</div>
								<div class="img_box">
									<img src="/resources/images/index/sec1_banner_img01.png" alt="" />
								</div>
							</div>
							<div class="swiper-slide">
								<div class="txt_box">
									<h3>리뉴얼 사이트 오픈!</h3>
									<p>더욱 새로워진 다우IDC</p>
									<a href="/support/news-view.do?idx=722">바로가기</a>
								</div>
								<div class="img_box">
									<img src="/resources/images/index/sec1_banner_img02.jpg" alt="" />
								</div>
							</div>
						</div>
						<!-- Add Pagination -->
						<div class="swiper-pagination"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="sec2">
			<div id="player_wrap" class="player_wrap">
				<div id="player"></div>
			</div>
			<div class="sec2_event_area">
				<div class="inner">
					<c:if test="${!empty notice2Week }">
					<div class="event">
						<ul id="eventList">
							<c:forEach items="${notice2Week }" var="list" varStatus="count">
								<li ${count.count == 1 ? 'class="on"' : '' } >
									<a href="/support/news-view.do?idx=${list.idx }">
										<span>${list.categoryStr }</span>
										<p>${list.title }</p>
									</a>
								</li>
							</c:forEach>
						</ul>
					</div>
					</c:if>
					<div class="tab_area">
						<ul id="sec1HoverList">
							<li class="list1">
								<div class="sec1_btn_area"><span class="ico"></span><a href="/idc-service/colocation.do">자세히보기</a><a href="/support/contact.do">상담하기</a></div>
								<span class="txt1">코로케이션<span class="txt2">안정적이고 전문적인<br>서버관리</span></span>
							</li>
							<li class="list2">
								<div class="sec1_btn_area"><span class="ico"></span><a href="/idc-service/hosting.do">자세히보기</a><a href="/support/contact.do">상담하기</a></div>
								<span class="txt1">서버 호스팅<span class="txt2">전문가와의 맞춤서버 상담</span></span>
							</li>
							<li class="list3">
								<div class="sec1_btn_area"><span class="ico"></span><a href="/cloud-service/server.do">자세히보기</a><a href="/support/contact.do">상담하기</a></div>
								<span class="txt1">클라우드<span class="txt2">필요한 만큼 쓰고 내는<br>손쉬운 클라우드</span></span>
							</li>
							<li class="list4">
								<div class="sec1_btn_area"><span class="ico"></span><a href="/additional-service/cdn.do">자세히보기</a><a href="/support/contact.do">상담하기</a></div>
								<span class="txt1">CDN<span class="txt2">빠르고 안정적인<br>데이터 전송</span></span>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="sec3">
			<div class="inner">
				<div class="tit_box">
					<h3><img src="/resources/images/index/sec2_tit.png" alt="" /></h3>
					<p>정보 입력 후 상담을 신청하시면<br>365일 24시간 빠르고 친절하게<br>답변해 드리겠습니다.</p>
				</div>
				<div class="consultation_area">
					<form id="formReg" action="/support/requestFastContact.do" method="post">
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
					<input type="hidden" name="cphone" />
					<input type="hidden" name="email" title="이메일"/>
						<ul class="select_area">
							<li>
								<input type="radio" name="productName" id="consultation1" class="consultation1" checked="checked" value="코로케이션" title="상품명"/>
								<label for="consultation1" class="consultation1">코로케이션</label>
							</li>
							<li><input type="radio" name="productName" id="consultation2" value="서버 호스팅" title="상품명"/><label for="consultation2">서버 호스팅</label></li>
							<li><input type="radio" name="productName" id="consultation3" value="클라우드" title="상품명"/><label for="consultation3">클라우드</label></li>
							<li><input type="radio" name="productName" id="consultation4" value="CDN" title="상품명"/><label for="consultation4">CDN</label></li>
						</ul>
						<div class="write_area">
							<ul>
								<li>
									<label for="companyName">회사명</label>
									<input type="text" id="companyName" name="companyName" title="회사명" maxlength="50">
								</li>
								<li class="mgl_10">
									<label for="name">담당자</label>
									<input type="text" id="name" name="name" title="담당자" maxlength="25">
								</li>
								<li>
									<label for="cphone">연락처</label>
									<div>
										<select name="" id="cphone" title="연락처">
											<option value="010">010</option>
											<option value="011">011</option>
											<option value="016">016</option>
											<option value="017">017</option>
											<option value="018">018</option>
											<option value="019">019</option>
										</select><span>-</span><input type="text" class="num_check w60" id="phoneNum1" title="연락처 중간자리" maxlength="4"><span>-</span><input type="text" class="num_check w60" id="phoneNum2" title="연락처 끝자리" maxlength="4"></div>
								</li>
								<li class="mgl_10">
									<label for="email1">이메일</label>
									<div><input type="text" class="w60" id="email1" title="이메일" maxlength="20"><span>@</span><input type="text" class="w100" id="email2" title="이메일" maxlength="20"></div>
								</li>
							</ul>
						</div>
						<div class="bottom_area">
							<input type="checkbox" id="agree"><label for="agree" class="custom_checkbox">개인정보수집동의</label>
							<a href="/membership/policy/privacy.do" class="btn_agree_content_view" target="_balnk">내용보기</a>
							<a href="javascript:void(0);" id="btnConsultation"class="commonBtn3 type2 btn_consultation">상담 신청하기</a>
						</div>
					</form>
				</div>
				<div class="banner_area">
					<p class="txt1">다우IDC가<br>처음이신가요?</p>
					<p class="txt2">고객님께 필요한 서비스를<br>찾아드립니다!</p>
					<a href="/about/service-finder.do">맞춤 서비스 찾기</a>
				</div>
			</div>
		</div>
		<div class="sec4">
			<div class="inner">
				<div class="tit_area">
					<h3>고객의 성공을 지원하는 다우IDC</h3>
					<p>다우IDC/클라우드의 다양한 고객사례를 확인해보세요.</p>
				</div>
				<div class="swiper-container swiper-container-2">
					<div class="swiper-wrapper">
						<div class="swiper-slide">
							<a href="/about/usercase-view1.do">
								<img src="/resources/images/index/sec3_img01.jpg" alt="" />
								<div class="img_area"><img src="/resources/images/index/sec3_ico01.jpg" alt="키움증권 logo" /></div>
								<p>접속 서버를 배치해<br>시스템 안정성을 확보</p>
							</a>
						</div>
						<div class="swiper-slide">
							<a href="/about/usercase-view2.do">
								<img src="/resources/images/index/sec3_img02.jpg" alt="" />
								<div class="img_area"><img src="/resources/images/index/sec3_ico02.jpg" alt="한국정보인증 logo" /></div>
								<p>전용회선인 점 대 점 방식의<br>고정 통신회선 연결</p>
							</a>
						</div>
						<div class="swiper-slide">
							<a href="/about/usercase-view3.do">
								<img src="/resources/images/index/sec3_img03.jpg" alt="" />
								<div class="img_area"><img src="/resources/images/index/sec3_ico03.png" alt="KOREACENTER logo" /></div>
								<p>대량의 장비 관리를 위한<br>맞춤형 코로케이션, 보안 서비스 제공</p>
							</a>
						</div>
						<div class="swiper-slide">
							<a href="/about/usercase-view4.do">
								<img src="/resources/images/index/sec3_img04.jpg" alt="" />
								<div class="img_area"><img src="/resources/images/index/sec3_ico04.png" alt="gettyimagesKOREA logo" /></div>
								<p>전문 컨설팅을 통한<br>고성능 스토리지 도입</p>
							</a>
						</div>
						<div class="swiper-slide">
							<a href="/about/usercase-view5.do">
								<img src="/resources/images/index/sec3_img05.jpg" alt="" />
								<div class="img_area"><img src="/resources/images/index/sec3_ico05.png" alt="BANK MEDIA logo" /></div>
								<p>IX망과 유연한 대역폭 제공으로<br>인프라 운영 부담 감소</p>
							</a>
						</div>
						<div class="swiper-slide">
							<a href="/about/usercase-view6.do">
								<img src="/resources/images/index/sec3_img06.jpg" alt="" />
								<div class="img_area"><img src="/resources/images/index/sec3_ico06.png" alt="CYFUN logo" /></div>
								<p>대용량 CDN 서비스를 제공하여<br>트래픽 폭주 현장 대비</p>
							</a>
						</div>
						<div class="swiper-slide">
							<a href="/about/usercase-view7.do">
								<img src="/resources/images/index/sec3_img07.jpg" alt="" />
								<div class="img_area"><img src="/resources/images/index/sec3_ico07.png" alt="키다리이엔티 logo" /></div>
								<p>DNS 기반으로 안정성을 높이고,<br>확장성을 고려하여 클라우드 사용</p>
							</a>
						</div>
						<div class="swiper-slide">
							<a href="/about/usercase-view8.do">
								<img src="/resources/images/index/sec3_img08.jpg" alt="" />
								<div class="img_area"><img src="/resources/images/index/sec3_ico08.png" alt="우리다문화장학재단 logo" /></div>
								<p>IT 인프라 구축을 위한 최적화된<br>코로케이션 서비스 제공</p>
							</a>
						</div>
					</div>
					<!-- Add Pagination -->
					<div class="swiper-pagination"></div>
				</div>
			</div>
		</div>
		<div class="sec5">
			<div class="inner">
				<div class="tit_area">
					<h3>최신 설비 전문 데이터 센터</h3>
					<p>IT인프라를 안정적이고 효율적으로 운영하실 수 있도록 최적의 환경을 제공합니다.</p>
					<a href="/about/intro.do">다우IDC소개 바로가기</a>
				</div>
				<ul>
					<li class="sec5_ico1">뛰어난 접근성</li>
					<li class="sec5_ico2">최신 인프라</li>
					<li class="sec5_ico3">24시 기술지원</li>
					<li class="sec5_ico4">업계 최저가 서비스</li>
				</ul>
			</div>
		</div>
		<div class="sec6">
			<div class="inner">
				<div class="top_cont">
					<h3><a href="/support/news-list.do">공지사항</a></h3>
					<ul>
						<c:forEach items="${notice }" var="list">
							<li><a href="/support/news-view.do?idx=${list.idx }"><span class="date"><small class="month"><fmt:formatDate pattern="yyyy.MM" value="${list.distributeDate }"/> </small><span class="day"><fmt:formatDate pattern="dd" value="${list.distributeDate }"/></span></span><span class="notice_cont">${list.title }</span></a></li>
						</c:forEach>
					</ul>
				</div>
				<div class="bottom_cont">
					<div class="swiper">
						<div class="swiper-container swiper-container-3">
							<div class="swiper-wrapper">
								<c:forEach items="${banner }" var="list">
									<div class="swiper-slide"><a href="${list.link }" ${list.outLinkYn == 'Y' ? 'target="_blank"' : '' }><img src="" alt=""></a></div>
								</c:forEach>
							</div>
							<!-- Add Pagination -->
							<div class="swiper-pagination"></div>
						</div>
					</div>
					<div class="right_banner">
						<a href="http://www.daouoffice.com" target="_blank"><img src="/resources/images/index/sec5_banner_01.jpg" alt="" /></a>
						<a href="http://www.halfdomain.co.kr/index" target="_blank"><img src="/resources/images/index/sec5_banner_02.jpg" alt="" /></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- footer briefpop -->
	<%@ include file="include/footer.jsp" %>
	<!-- //footer briefpop-->
</div>
<!-- //wrap -->
<%@ include file="include/js.jsp" %>
<script>
//오늘하루보지않기
var cookieTitle;
function setCookie( name, value, expiredays ) {
  var todayDate = new Date();
  todayDate.setDate( todayDate.getDate() + expiredays );
  document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + todayDate.toGMTString() + ";"
};
function pop_close(count){
	var classStr = 'layer_popup'+count;
	var inputStr = "Ntoday" + count;
	$('.'+classStr).hide();
	if($('#'+inputStr).is(":checked") ==true){
		setCookie(inputStr,"done",1);
	};
};
var cookiedata = document.cookie;
$(".pop").each(function(index,item){
	var cookieName = "Ntoday"+(index+1);
	if(cookiedata.indexOf(cookieName + "=done")<0){
	    $(item).addClass("on");
	}else{
		$(item).removeClass("on")
	};
});

(function(){
	$(".num_check").on("keyup", function(e){
		var regNumber = /^[0-9]*$/;
		if(!regNumber.test($(this).val())) {
			alert("휴대번호를 정확히 기재해주세요");
			$(this).val($(this).val().replace(/[^0-9]/g, ""));
			return;
		}
	});
	$('#userId').change(function () {
		$('#userIdDupli').val('N');
	});
	$('#userEmail2Select').change(function () {
		if ($(this).val() != '') {
			$('#userEmail2').val($(this).val());
			$('#userEmail2').attr('readonly', true);
		}
		else {
			$('#userEmail2').val('');
			$('#userEmail2').attr('readonly', false);
		}
	});
	$("#sec1HoverList").find("li").each(function(index, item){
		$(item).on("mouseenter mouseleave" , function(e){
			if(e.type =="mouseenter"){
				$(this).find(".ico").stop().animate({
					bottom: 80
				}, 500 ,"easeOutCirc");
			}
			if(e.type=="mouseleave"){
				$(this).find(".ico").stop().animate({
					bottom: 40
				}, 500 ,"easeOutCirc")
			}
		})
	});
	$("#agree").on("change" , function(){
		if($(this).is(":checked")){
			$(this).parent("label").addClass("on");
		}else{
			$(this).parent("label").removeClass("on");
		}
	});
	$("#btnConsultation").click(function(){
		if($(".select_area").find("input:checked").size() == 0){
			alert("1번 질의 항목에 답변하지 않으셨습니다.");
			$(".consultation1").focus();
			return false;
		}
		if(!$("#companyName").required()) return false;
		if(!$("#name").required()) return false;
		if(!$("#phoneNum1").required()) return false;
		if(!$("#phoneNum1").isnum()) return false;
		if(!$("#phoneNum2").required()) return false;
		if(!$("#phoneNum2").isnum()) return false;
		if(!$("#email1").required()) return false;
		if(!$("#email2").required()) return false;
		
		var phone = $('#cphone').val()+'-'+$('#phoneNum1').val()+'-'+$('#phoneNum2').val();
		var email = $('#email1').val()+'@'+$('#email2').val();
		$('input[name="cphone"]').val(phone);
		$('input[name="email"]').val(email);	
		if(!$('input[name="email"]').isemail())	return false;
		
		if(!$("#agree").is(":checked")){
			alert("개인정보 수집을 동의해주세요.");
			$("#agree").focus();
			return false;
		}
		
		if(!confirm("신청하시겠습니까?")) return false;
		$("#formReg").submit();
	});

	var swiper = new Swiper('.swiper-container-1', {
		pagination : '.swiper-pagination',
		autoplay: 7000,
		paginationClickable: true
	});
	var swiper = new Swiper('.swiper-container-2', {
		slidesPerView: 4,
		spaceBetween: 30,
		slidesPerGroup: 4,
		loop: true,
		pagination : '.swiper-pagination',
		paginationClickable: true
	});
	var swiper = new Swiper('.swiper-container-3', {
		pagination : '.swiper-pagination',
		paginationClickable: true
	});

	var cnt= 0;
	var $eventListLi = $("#eventList").find("li");
	var eventListLength= $eventListLi.length;
	setInterval(function(){
		cnt++;
		if(cnt > eventListLength-1){
			cnt = 0;
		}
		$eventListLi.eq(cnt).fadeIn().siblings().fadeOut();
	},5000);
	
	var randomVideo = Math.floor(Math.random() * 3) + 1;
	$("#player").jPlayer({
		ready: function () {
			$(this).jPlayer("setMedia", {
				m4v: "/resources/file/main_0" + randomVideo + ".mp4",
			}).jPlayer("play");
		},
		swfPath: "/resources/file/jquery.jplayer.swf",
		supplied: "m4v",
		size: {
			width: "1920",
			height: "100%",
			cssClass: ""
		},
		backgroundColor: "#fff",
		useStateClassSkin: true,
		autoBlur: false,
		smoothPlayBar: true,
		keyEnabled: true,
		remainingDuration: false,
		toggleDuration: false,
		loop: true,
		autoPlay:true,
		volume: 0,
		cssSelectorAncestor: "#player_wrap"
	});
	
	$("#player").find("video").attr("playsinline", "playsinline");
	$("#player").find("video").attr("muted", "muted");
	
	$("#player_wrap").addClass("bg0" + randomVideo + "");
	
	//팝업창 드래그
	$(".pop ").draggable({"cancel":"#header", containment:"#container", scroll:false});

	$(window).resize(function () {
		winW = $(window).width();
		if ($(window).width() > 1600) {
			$("#player").css({"width": winW * 1.2});
		}
	}).trigger("resize");
})();
</script>
</body>
</html>