package org.example.demo.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Base64.Encoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.example.demo.dto.Admin;
import org.example.demo.dto.TokenRequestResult;
import org.example.demo.dto.UserInfoResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class OAuthServiceImpl implements OAuthService {
	private static final Logger log = LoggerFactory.getLogger(OAuthServiceImpl.class);
	
	private String authorizationRequestHeader;
	
	@Value("#{applicationProperties['server.oauth.domain']}")
	private String OAUTH_DOMAIN;
	
	@Value("#{applicationProperties['server.local.domain']}")
	private String LOCAL_DOMAIN;
	
	@Value("#{applicationProperties['security.oauth2.client.client-id']}")
	private String OAUTH_CLIENT_ID;
	
	@Value("#{applicationProperties['security.oauth2.client.client-secret']}")
	private String OAUTH_CLIENT_SECRET;
	
	@Value("#{applicationProperties['security.oauth2.client.access-token-uri']}")
	private String ACCESS_TOKEN_URL;
	
	@Value("#{applicationProperties['security.oauth2.client.resource.user-info-uri']}")
	private String USER_INFO_URL;
	
	
	
	@Override
	public TokenRequestResult requestAccessTokenToAuthServer(String code, HttpServletRequest request) {
		//6. 발급 받은 코드 기반으로 Access Token 요청
		TokenRequestResult tokenRequestResult = requestAccessTokenToAuthServer(code);
		log.debug("\n## tokenResult : '{}'\n", tokenRequestResult);
		
		if (tokenRequestResult.getError() != null) {
			return tokenRequestResult;
		}
		
		//7. 발급받은 AccessToken을 기반으로 사용자 정보 요청
		UserInfoResponse userInfoResponse = requestUserInfoToAuthServer(tokenRequestResult.getAccessToken());
		if (userInfoResponse.getResult() == false) {
			tokenRequestResult.setError(userInfoResponse.getMessage());
			return tokenRequestResult;
		}
		
		this.securityLoginWithoutLoginForm(request, userInfoResponse);
		
		return tokenRequestResult;
	}
	
	private TokenRequestResult requestAccessTokenToAuthServer(String code) {
		String authorizationHeader = getAuthorizationRequestHeader();
		
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put("grant_type", "authorization_code");
		paramMap.put("redirect_uri", getOAuthRedirectUri());
		paramMap.put("code", code);
		
		HttpPost post = buildHttpPost(ACCESS_TOKEN_URL, paramMap, authorizationHeader);
		TokenRequestResult result = executePostAndParseResult(post, TokenRequestResult.class);
		
		return result;
	}
	
	private UserInfoResponse requestUserInfoToAuthServer(String token) {
		String authorizationHeader = getAuthorizationRequestHeader();
		
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put("access_token", token);
		
		HttpPost post = buildHttpPost(USER_INFO_URL, paramMap, authorizationHeader);
		UserInfoResponse result = executePostAndParseResult(post, UserInfoResponse.class);
		
		return result;
	}
	
	private <T> T executePostAndParseResult(HttpPost post, Class<T> clazz) {
		T result = null;
		try {
			HttpClient client = HttpClientBuilder.create().build();
			
			HttpResponse response = client.execute(post);
			BufferedReader rd = new BufferedReader(
				new InputStreamReader(response.getEntity().getContent()));
	
			StringBuffer resultBuffer = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				resultBuffer.append(line);
			}
			log.debug("\n## response body : '{}'", resultBuffer.toString());
			
			ObjectMapper mapper = new ObjectMapper();
			result = mapper.readValue(resultBuffer.toString(), clazz);
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
		
		return result;
	}

	private HttpPost buildHttpPost(String reqUrl, Map<String, String> paramMap, String authorizationHeader) {
		log.debug("\n## in buildHttpPost() reqUrl : {}", reqUrl);
		HttpPost post = new HttpPost(reqUrl);
		if (authorizationHeader != null) {
			post.addHeader("Authorization", authorizationHeader);
		}
		
		List<NameValuePair> urlParameters = new ArrayList<>();
		for (Map.Entry<String, String> entry : paramMap.entrySet()) {
			urlParameters.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
		}
		
		try {
			post.setEntity(new UrlEncodedFormEntity(urlParameters));
		} catch (UnsupportedEncodingException e) {
			log.error(e.getMessage(), e);
		}
		return post;
	}
	
	private String getAuthorizationRequestHeader() {
		if (authorizationRequestHeader == null) {
			setAuthroizationRequestHeader();
		}
		return authorizationRequestHeader;
	}
	
	private void setAuthroizationRequestHeader() {
		// "ClinetID:ClientSecret" 을 Base64로 암호화 하여 Authorization Header로 생성
		Encoder encoder = Base64.getEncoder();
		try {
			String toEncodeString = String.format("%s:%s", OAUTH_CLIENT_ID, OAUTH_CLIENT_SECRET);
			authorizationRequestHeader = "Basic " + encoder.encodeToString(toEncodeString.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			log.error(e.getMessage(), e);
		}
	}
	
	private String extractTokenId(String value) {
		if (value == null) {
			return null;
		}
		try {
			MessageDigest digest = MessageDigest.getInstance("MD5");
			byte[] bytes = digest.digest(value.getBytes("UTF-8"));
			return String.format("%032x", new BigInteger(1, bytes));
		}
		catch (NoSuchAlgorithmException e) {
			throw new IllegalStateException("MD5 algorithm not available.  Fatal (should be in the JDK).");
		}
		catch (UnsupportedEncodingException e) {
			throw new IllegalStateException("UTF-8 encoding not available.  Fatal (should be in the JDK).");
		}
	}
	
	private void securityLoginWithoutLoginForm(HttpServletRequest req, UserInfoResponse userInfoResponse) { 
		Admin admin = new Admin();
		admin.setId(userInfoResponse.getUserPrincipal());
		
		List<GrantedAuthority> list = new ArrayList<GrantedAuthority>();
		list.add(new SimpleGrantedAuthority("ROLE_USER"));
	    SecurityContext sc = SecurityContextHolder.getContext();
	    sc.setAuthentication(new UsernamePasswordAuthenticationToken(admin, null, list));
	    HttpSession session = req.getSession(true);
	    session.setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, sc);
	}
	
	private String getOAuthRedirectUri() {
		return LOCAL_DOMAIN+"/oauthCallback";
	}
}
