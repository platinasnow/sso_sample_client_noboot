package org.example.demo.service;

import javax.servlet.http.HttpServletRequest;

import org.example.demo.dto.Response;
import org.example.demo.dto.TokenRequestResult;

public interface OAuthService {
	TokenRequestResult requestAccessTokenToAuthServer(String code, HttpServletRequest request);

}