package org.example.demo.controller;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.example.demo.dto.TokenRequestResult;
import org.example.demo.service.OAuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class SsoController {
	private static final Logger log = LoggerFactory.getLogger(SsoController.class);
	
	@Autowired private OAuthService oauthService;
	
	@Value("#{applicationProperties['server.oauth.domain']}")
	private String OAUTH_DOMAIN;
	
	@Value("#{applicationProperties['server.local.domain']}")
	private String LOCAL_DOMAIN;
	
	@Value("#{applicationProperties['security.oauth2.client.client-id']}")
	private String OAUTH_CLIENT_ID;
	
	@Value("#{applicationProperties['security.oauth2.client.user-authorization-uri']}")
	private String AUTHORIZATION_URL;
	
	@RequestMapping(value="/sso", method=RequestMethod.GET)
	public String sso(HttpServletRequest request) {
		//1. 로그인 시작 - Authorization Server에 Code값을 전송하여 인가 여부 확인 및 리다이렉트
		String state = UUID.randomUUID().toString();
		request.getSession().setAttribute("oauthState", state);
		
		// http://AUTHORIZATION_URL?response_type=code&client_id=[ClientId]&redirect_uri=[RedirectUrl]&scope=[Scope]&=state=[State] 의 값으로 구성
		StringBuilder builder = new StringBuilder();
		builder.append("redirect:");
		builder.append(AUTHORIZATION_URL);
		builder.append("?response_type=code");
		builder.append("&client_id=");
		builder.append(OAUTH_CLIENT_ID);
		builder.append("&redirect_uri=");
		builder.append(getOAuthRedirectUri());
		builder.append("&scope=");
		builder.append("read");
		builder.append("&state=");
		builder.append(state);
		return builder.toString();
	}
	
	@RequestMapping(value="/oauthCallback", method=RequestMethod.GET)
	public String oauthCallback(Model model, String code, String state, HttpServletRequest request) {
		String oauthState = (String)request.getSession().getAttribute("oauthState");
		request.getSession().removeAttribute("oauthState");
		log.debug("\n## code, oauthState, state : {}, {}, {}", code, oauthState, state);
		
		//5. Authority Server와의 통신 결과 code가 일치하는지 검사
		TokenRequestResult tokenRequestResult = null;
		if (oauthState == null || oauthState.equals(state) == false) {
			tokenRequestResult = new TokenRequestResult();
			tokenRequestResult.setError("not matched state");
		}
		else {
			//발급 받은 코드 기반으로 Access Token 요청
			tokenRequestResult = oauthService.requestAccessTokenToAuthServer(code, request);
		}
		if (tokenRequestResult.getError() == null) {
			return "redirect:/";
		}
		else {
			model.addAttribute("result", tokenRequestResult);
			return "authResult";
		}
	}
	
	@PostMapping(value = "/doLogout")
	public String logout(HttpSession session) {
		session.invalidate();
		return "redirect:"+OAUTH_DOMAIN+"/exit?redirectUrl="+LOCAL_DOMAIN;
	}
	
	
	private String getOAuthRedirectUri() {
		return LOCAL_DOMAIN+"/oauthCallback";
	}
	
}
